﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MovieDataStore.Models
{
    public class Franchise
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(300)]
        public string Description { get; set; }
        //[MaxLength(100)]
        //public string MovieId { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }

    //TODO: Seed data into a test database
}
