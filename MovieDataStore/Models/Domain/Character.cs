﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MovieDataStore.Models
{
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(100)]
        public string Gender  { get; set; }
        [MaxLength(400)]
        public string PictureUrl { get; set; }
        //public int MoviesId { get; set; }
        //public ICollection<Movie> Movies { get; set; }
        //public int MovieId { get; set; }
        //public Movie Movie { get; set; }
    }
}
