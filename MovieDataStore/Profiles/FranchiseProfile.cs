﻿using AutoMapper;
using MovieDataStore.Models;
using MovieDataStore.Models.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDataStore.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile() 
        {
            CreateMap<Franchise, FranchiseReadDTO>();
        }
    }
}
