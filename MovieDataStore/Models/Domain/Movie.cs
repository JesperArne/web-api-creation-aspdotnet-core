﻿using MovieDataStore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDataStore
{
    public class Movie
    {

        public int Id { get; set; }

        [MaxLength(50)]
        public string Title { get; set; }


        [MaxLength(50)]
        public string Genre { get; set; } // Can be comma seperated in a by converting this prop in to List // migration does not support type of list bc it is not a primitive


        public int ReleaseYear { get; set; }

        [MaxLength(150)] 
        public string Director { get; set; }
         [MaxLength(200)]
        public string PictureUrl { get; set; }
       
        [MaxLength(200)]
        public string TrailerUrl { get; set; }

        //public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }

        //public int CharactorId { get; set; }

        //public Character Characters { get; set; }

    }
}
